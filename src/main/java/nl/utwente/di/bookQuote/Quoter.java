package nl.utwente.di.bookQuote;
import java.util.HashMap;
public class Quoter {
    HashMap<String,Double> prices;
    public Quoter(){
        prices = new HashMap<String,Double>();
        prices.put("1",10.0);
        prices.put("2",45.0);
        prices.put("3",20.0);
        prices.put("4",35.0);
        prices.put("5",50.0);
    }
    double getBookPrice(String isbn) {
        return prices.get(isbn);
    }
}
